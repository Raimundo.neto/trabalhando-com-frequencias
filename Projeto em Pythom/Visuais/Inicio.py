from Visuais.testes import janela#importação da classe visual da proxima janela
import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk,Gdk
class Inicio:#classe da janela inical
    def enviar(self,evt):#metodo do botão para alterar as janelas
        self.janelaB.hide()#metodo para esconder a janela atual
        self.janelaB.destroy()#metodo para destruir a janela atual
        self.app = janela()# criação da instancia para iniciar a proxima janela
    def __init__(self):# construtor da janela
        #começaremos a partir daqui
        builder = Gtk.Builder()# instacia da Gtk Builder
        builder.add_from_file("Visuais/inicio.glade")#definindo qual o local e qual arquivo glade que deve ser usado pela aplicação na hora de iniciar 
        self.janelaB = builder.get_object("telaDiag")#instanciado a janela
        self.butaoG = builder.get_object("bntEnviar")#instanciando o butão 
        self.css()#adicionando efetivamente o css as classe visuai
        builder.connect_signals(self)#conectando os sinais dos butoes e outros sinais como o de fechar e ocultar a janela
        self.janelaB.show_all()#função que mostra todas os widgets presentes na janela
        self.janelaB.connect("destroy",Gtk.main_quit)# conectando ao metodo de fechar a janela
        Gtk.main()#Iniciar o Gtk main
    def css(self):
        css = b"""
        .telaDiag{
            background-color: #1e417a;
             
        }
        .bntEnviar{
            background-color: #FFFFFF;
            border-radius: 10px;
        }   
        """
        style_provider = Gtk.CssProvider()
        style_provider.load_from_data(css)
        Gtk.StyleContext.add_provider_for_screen(Gdk.Screen.get_default(),
        style_provider,Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)