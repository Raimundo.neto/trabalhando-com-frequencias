from Controle.primeiroControle import control#importação da classe de controle control
from Controle.segundoControle import controlDois#importação da classe de controle controlDois
import gi
from Modelos.Planilha import Planilha
from Visuais.Tabela import Tabela
gi.require_version("Gtk","3.0")
from gi.repository import Gtk,Gdk
class janela:
    
    def Areturno(self,evt):
        self.d = control()#composição da classe de controle control
        self.d.deletar()#chamada do metodo para apagar os dados
        self.janelaE.hide()#metodo para ocultar a dialog janelaE
        self.janela.show_all()#função que mostra todas os widgets presentes na janela
        self.matricula.set_text('')#função que limpa os campos
        self.usuario.set_text('')#função que limpa os campos
        self.Serie.set_text('')#função que limpa os campos
        self.turma.set_text('')#função que limpa os campos
        self.numero.set_text('')#função que limpa os campos
        self.data.set_text('')#função que limpa os campos
        self.materia.set_text('')#função que limpa os campos
        self.horario.set_active(9)#função que limpa os campos
    
    def returno(self,evt):
        self.c.armazenar(self.usuario.get_text(),self.Serie.get_text(),self.matricula.get_text(),self.turma.get_text(),self.numero.get_text(),self.data.get_text(),self.horario.get_active_text())#estrutura para salvar os dados na memoria
        self.janelaE.hide()#metodo para ocultar a dialog janelaE
        self.janela.show_all()#função que mostra todas os widgets presentes na janela
        self.matricula.set_text('')#função que limpa os campos
        self.usuario.set_text('')#função que limpa os campos
        self.Serie.set_text('')#função que limpa os campos
        self.turma.set_text('')#função que limpa os campos
        self.numero.set_text('')#função que limpa os campos
        self.data.set_text('')#função que limpa os campos
        self.materia.set_text('')#função que limpa os campos
        self.horario.set_active(9)
    
    def cancelar(self,evt):#metodo para apagar as informações dos campos de texto
        self.matricula.set_text('')#função que limpa os campos
        self.usuario.set_text('')#função que limpa os campos
        self.Serie.set_text('')#função que limpa os campos
        self.turma.set_text('')#função que limpa os campos
        self.numero.set_text('')#função que limpa os campos
        self.data.set_text('')#função que limpa os campos
        self.materia.set_text('')#função que limpa os campos
        self.horario.set_active(9)#função que limpa os campos
    
    def encap(self,evt):
        self.c = controlDois()#composição da classe de controle controlDois
        if self.c.teste(self.usuario.get_text(),self.Serie.get_text(),self.matricula.get_text(),self.turma.get_text(),self.numero.get_text(),self.data.get_text()) == True:#estrutura para conferir se algum campo esta vazio
            self.janelaE.show_all()#metodo para mostrar a dialog janelaE
        else:
            self.janelaB.show_all()#metodo para mostrar a dialog janelaB
    def retornar(self,evt):#metodo do evento do butão para retornar a janela principal
        self.janelaB.hide()#metodo para ocultar a dialog janelaB

    def __init__(self):#construtor da janela
        #começaremos a partir daqui
        builder = Gtk.Builder()#instacia da Gtk Builder
        builder.add_from_file("Visuais/trab.glade")#definindo qual o local e qual arquivo glade que deve ser usado pela aplicação na hora de iniciar 
        self.janela = builder.get_object("telaTrab")#instancia da janela principal
        self.janelaB = builder.get_object("telaConfirm")#instancia da Dialog erro de envio
        self.janelaE = builder.get_object("telaFinal")#instancia da Dialog tela final
        self.usuario = builder.get_object("tfNome")#instancia do campo de texto para o Nome
        self.Serie = builder.get_object("tfSerie")#instancia do campo de texto para a Serie
        self.matricula = builder.get_object("tfMatricula")#instancia do campo de texto para o Numero
        self.turma = builder.get_object("tfTurma")#instancia do campo de texto para a turma
        self.data = builder.get_object("tfData")#instancia do campo de texto para a data
        self.horario = builder.get_object("cmbHorario")#instancia do campo de texto para o 'Horario
        self.materia = builder.get_object("tfMateria")#instancia do campo de texto para a Materia
        self.numero = builder.get_object("tfNumero")#instancia do campo de texto para o numero
        self.lb = builder.get_object("lb1")#instancia da label para mostrar os dados para o usuario
        self.lbB = builder.get_object("lb2")#instancia da label para mostrar os dados para o usuario
        self.lbC = builder.get_object("lb3")#instancia da label para mostrar os dados para o usuario
        self.lbD = builder.get_object("lb4")#instancia da label para mostrar os dados para o usuario
        self.lbF = builder.get_object("lb6")#instancia da label para mostrar os dados para o usuario
        self.lbG = builder.get_object("lb7")#instancia da label para mostrar os dados para o usuario
        self.lbH = builder.get_object("lb8")#instancia da label para mostrar os dados para o usuario
        self.lbI = builder.get_object("lb9")#instancia da label para mostrar os dados para o usuario
        self.lbJ = builder.get_object("lb10")#instancia da label para mostrar os dados para o usuario
        self.butaoA = builder.get_object("btnVer")#instancia do butão a
        self.butaoC = builder.get_object("btnCancelar")#instancia do butão C
        self.butaoB = builder.get_object("btnEnviar")# instancia do butão B
        self.butaoD = builder.get_object("bntReturn")#instancia do butão D
        self.butaoE = builder.get_object("bntRetornar")#instancia  do butão E
        self.butaoF = builder.get_object("bntRetroceder")#instancia do butão F
        self.butaoG = builder.get_object("bntFinal")#instancia do butão G
        self.butaoH = builder.get_object("bntDestroy")#instancia do butão H
        self.css()#adicionando efetivamente o css a classe visuai
        builder.connect_signals(self)#conectando os sinais dos butoes e outros sinais como o de fechar e ocultar a janela
        self.janela.show_all()#função que mostra todas os widgets presentes na janela
        self.janela.connect("destroy",Gtk.main_quit)# conectando ao metodo de fechar a janela
        Gtk.main()#Iniciar o Gtk main
        
    def css(self):
        css = b"""
        .telaConfirm{
            background-color: #1e417a;
        }
        .telaTrab{
            background-color: #1e417a;  
        }   
        .btnVer{
            background-color: #FFFFFF;
            border-radius: 10px;
        }
        .bntEnviar{
            background-color: #FFFFFF;
            border-radius: 10px;
        }
        .bntCancelar{
            background-color: #FFFFFF;
            border-radius: 10px;
        }
        .bntReturn{
            background-color: #FFFFFF;
            border-radius: 10px;
        }
        .bntRetornar{
            background-color: #FFFFFF;
            border-radius: 10px;
        }
        .bntRetroceder{
            background-color: #FFFFFF;
            border-radius: 10px;        
        }
        .bntFinal{
            background-color: #000000;
            color:#ffffff;
            border-radius: 10px;
        }
        .bntDestroy{
            background-color: #000000;
            color:#ffffff;
            border-radius: 10px;
        }
        """
        style_provider = Gtk.CssProvider()
        style_provider.load_from_data(css)
        Gtk.StyleContext.add_provider_for_screen(Gdk.Screen.get_default(),
        style_provider,Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)                