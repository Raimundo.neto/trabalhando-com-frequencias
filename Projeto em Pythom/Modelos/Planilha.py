class Planilha:
    __id = 0
    __nomeDoAluno = ""#Atributo privado nome do aluno
    __numero = 0#Atributo privado Numero da chamada
    __serie = ''#Atributo ṕrivado serie
    __turma = ''#Atributo privado turma
    __faltas = 0#Atributo privado faltas
    __Matricula = 0#Atributo Privado Matricula atual
    __materia = ""
    def setTurma(self,turma):#Encapsulamento de Turma mais especificamente o metodo set() que e usado para alterar o valor
        self.__turma = turma
    def getTurma(self):#Encapsulamento de Turma mais especificamente o metodo get() que e usado para ver o valor
        return self.__turma    
    def setNomeDoAluno(self,nomeDoAluno):#Encapsulamento de NomeDoAluno mais especificamente o metodo set() que e usado para alterar o valor
        self.__nomeDoAluno = nomeDoAluno
    def getNomeDoAluno(self):#Encapsulamento de NomeDoAluno mais especificamente o metodo get() que e usado para ver o valor
        return self.__nomeDoAluno
    def setNumero(self,numero):#Encapsulamento de Numero mais especificamente o metodo set() que e usado para alterar o valor
        self.__numero = numero
    def getNumero(self):#Encapsulamento de Numero mais especificamente o metodo get() que e usado para ver o valor
        return self.__numero
    def setSerie(self,serie):#Encapsulamento de Serie mais especificamente o metodo set() que e usado para alterar o valor
        self.__serie = serie
    def getSerie(self):#Encapsulamento de Serie mais especificamente o metodo get() que e usado para ver o valor
        return self.__serie
    def setFaltas(self,faltas):#Encapsulamento de Faltas mais especificamente o metodo set() que e usado para alterar o valor
        self.__faltas = faltas 
    def getFaltas(self):#Encapsulamento de Faltas mais especificamente o metodo get() que e usado para ver o valor
        return self.__faltas
    def setMatricula(self,m):#Encapsulamento de diaAtual mais especificamente o metodo set() que e usado para alterar o valor
        self.__Matricula = m
    def getMatricula(self):#Encapsulamento de diaAtual mais especificamente o metodo get() que e usado para ver o valor
        return self.__Matricula
    def setMateria(self,mm):#Encapsulamento de diaAtual mais especificamente o metodo set() que e usado para alterar o valor
        self.__materia = mm
    def getMateria(self):#Encapsulamento de diaAtual mais especificamente o metodo get() que e usado para ver o valor
        return self.__materia
    def getId(self):#Encapsulamento de diaAtual mais especificamente o metodo get() que e usado para ver o valor
        return self.__id
    def setId(self,i):#Encapsulamento de diaAtual mais especificamente o metodo set() que e usado para alterar o valor
        self.__id = i