class Data:
    __dia = ''#Atributo Privado dia atual
    __mes = ''#Atributo Privado mes atual
    __ano = ''#Atributo Privado ano atual
    def setDia(self,d):#Encapsulamento de diaAtual mais especificamente o metodo set() que e usado para alterar o valor
        self.__dia = d
    def getDia(self):#Encapsulamento de diaAtual mais especificamente o metodo get() que e usado para ver o valor
        return self.__dia
    def setMes(self,me):#Encapsulamento de diaAtual mais especificamente o metodo set() que e usado para alterar o valor
        self.__mes = me
    def getMes(self):#Encapsulamento de diaAtual mais especificamente o metodo get() que e usado para ver o valor
        return self.__mes
    def setAno(self,a):#Encapsulamento de diaAtual mais especificamente o metodo set() que e usado para alterar o valor
        self.__ano = a
    def getAno(self):#Encapsulamento de diaAtual mais especificamente o metodo get() que e usado para ver o valor
        return self.__ano