from Controle.Conexao import Conexao
from Modelos.Planilha import Planilha
from Modelos.Data import Data
class BD:
    def iserirAluno(self,Planilha):
        try:
            con = Conexao()
            a = Planilha.getNomeDoAluno()
            b = Planilha.getMatricula()
            d = Planilha.getSerie()
            e = Planilha.getTurma()
            f = Planilha.getNumero()
            c = Planilha.getFaltas()
            k = Planilha.getMateria()
            sql = "INSERT INTO aluno(matricula,Numero,serie,turma,nome,faltas,materias) VALUES(%s,%s,%s,%s,%s,%s,%s);"
            cursor = con.getCon().cursor()#equivalente ao prepare
            valores = (b,f,d,e,a,c,k)#valores adicionados a tabela
            cursor.execute(sql,valores)#equivalente ao bindParam
            if con.getCon().commit():
                con.fecharConexao()
                return True
            else:
                con.fecharConexao()
                return False
        except Exception as e:
            print("erro geral",e)
    def iserirData(self,Planilha):
        try:
            con = Conexao()
            l = Planilha.getDia()
            m = Planilha.getMes()
            n = Planilha.getAno()
            sql = "INSERT INTO data(dia,mes,ano) VALUES(%s,%s,%s);"
            cursor = con.getCon().cursor()#equivalente ao prepare
            valores = (l,m,n)#valores adicionados a tabela
            cursor.execute(sql,valores)#equivalente ao bindParam
            if con.getCon().commit():
                con.fecharConexao()
                return True
            else:
                con.fecharConexao()
                return False
        except Exception as e:
            print("erro geral",e)
    def selecionarId(self,Planilha):
        lista = []
        try:
            conexao = Conexao()
            sql = "SELECT id FROM aluno;"
            cursor = conexao.getCon().cursor(dictionary=True)
            lista = []
            cursor.execute(sql)
            consulta = cursor.fetchall()
            for i in range(0, consulta.__len__(),1):
                Planilha.setId(consulta[i]['id'])
                lista.append(Planilha)
        except Exception as e:
            print("Erro:" + str(e))
        return lista
    def selecionarIdData(self,Planilha):
        lista = ""
        try:
            conexao = Conexao()
            sql = "SELECT id FROM data;"
            cursor = conexao.getCon().cursor(dictionary=True)
            lista = []
            cursor.execute(sql)
            consulta = cursor.fetchall()
            for i in range(0, consulta.__len__(),1):
                Planilha.setId(consulta[i]['id'])
                lista.append(Planilha)
        except Exception as e:
            print("Erro:" + str(e))
        return lista        
    def selecionarUm(self,abobrinha):
        try:
            usu=Planilha()
            conexao = Conexao()
            sql= "SELECT * FROM aluno WHERE id=%s;"
            cursor = conexao.getCon().cursor(dictionary=True)
            valores = (abobrinha)
            cursor.execute(sql,(valores,))
            consulta = cursor.fetchone()
            usu.setId(consulta['id'])
            usu.setMatricula(consulta['matricula'])
            usu.setNomeDoAluno(consulta['nome'])
            usu.setSerie(consulta['serie'])
            usu.setTurma(consulta['turma'])
            usu.setNumero(consulta['Numero'])
            usu.setMateria(consulta['materias'])
            usu.setFaltas(consulta['faltas'])
            return usu
        except Exception:
            return ""
    def selecionarData(self,abobrinha):
        try:
            usu=Data()
            conexao = Conexao()
            sql= "SELECT * FROM data WHERE id=%s;"
            valores = (abobrinha)
            cursor = conexao.getCon().cursor(dictionary=True)
            cursor.execute(sql,(valores,))
            consulta = cursor.fetchone()
            usu.setDia(consulta['dia'])
            usu.setMes(consulta['mes'])
            usu.setAno(consulta['ano'])
            return usu
        except Exception:
            return ""
    def Deletar(self,a):
        try:
            con = Conexao()
            sql = "DELETE FROM alunos WHERE id=%s;"
            cursor = con.getCon().cursor()
            valores = (a)
            cursor.execute(sql,valores)#equivalente ao bindParam
            if con.getCon().commit():
                    return True
            else:
                con.fecharConexao()
                return False
        except Exception as e:
            print("erro geral",e)