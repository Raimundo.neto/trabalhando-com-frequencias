from Controle.ControleBD import BD
from Modelos.Planilha import Planilha
from Modelos.Data import Data
class controlDois:#classe de controle
    def armazenar(self,a,b,c,d,e,f,i,g,h):#metodo para armazenar as informações do usuario
        self.c = BD()#composição da classe Do banco
        self.n = Planilha()#composição da classe modelo planilha  
        self.m = Data()#composição da classe modelo Data
        self.n.setNomeDoAluno(a)#metodo de encapsulamento
        self.n.setMatricula(c)#metodo de encapsulamento
        self.n.setTurma(d)#metodo de encapsulamento
        self.n.setSerie(b)#metodo de encapsulamento
        self.n.setNumero(e)
        self.m.setDia(i)#metodo de encapsulamento
        self.m.setMes(g)#metodo de encapsulamento
        self.m.setAno(h)#metodo de encapsulamento
        self.n.setMateria(f)#metodo de encapsulamento
        self.c.iserirAluno(self.n) #metodo de inserção de aluno
        self.c.iserirData(self.m)#metodo de inserção de data
        return True#retorno verdadeiro
    def teste(self,a,b,c,d,e,f,i,g):#metodo para testa se algumas das entradas não esta preençida
        if a == '' or b == '' or c == '' or d == '' or e == "" or f  == "" or i == "" or g == "":# estrutura condicional de teste para verificar se algum campo esta vazio
            return False#retorno falso caso a informação seja verdadeira
        else:#estrutura condicional
            return True#retorno verdadeiro caso a informação seja falsa
