# Autores: Everton e Neto
## Projeto: Gerenciamento de Faltas

 Nosso projeto tem como funcionalidade auxiliar os registros de **faltas** dos alunos 
 da instituição de ensino Paulo Petrola, visando a facilitação do trabalho
 de funcionários da instituição.
    
Usamos as linguagens **Java** e **Python**, juntamente com o Dia Diagram Editor.Nosso
projeto funciona em torno das seguintes **regras**:
  
   
* Se o aluno chegar durante o almoço é necessário ir à coordenação para não
levar falta o dia todo;
    
* Caso o aluno tenha 2 faltas ou mais, é necessário comunicar-se com os pais;
    
* Caso o aluno complete 10 dias completos de faltas é assionado o Conselho Tutelar
para a família, porém, caso o aluno tenha atestado médico comprovando  
suas ausências, as faltas serão justificadas.

## Temos como referência:

|   Sites   |  Função   |
|:--------:|:----------:|
|[Aluno Online](https://alunoonline.seduc.ce.gov.br/)| Aprimoramento de Front - end|
|[Alura Cursos](https://www.alura.com.br/)|Conhecimentos em PDO|