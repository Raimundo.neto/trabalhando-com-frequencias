package connectFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;



public class AlunoDAO {
	public static void create(AlunoBean b){
		Connection connection  = TesteCon.getConnection();
	 PreparedStatement stmt = null;
		try {
		stmt = connection.prepareStatement("INSERT INTO faltas(nomeAluno,numChamada,curso,serie,materia,data,numPais,qntFaltas)VALUES(?,?,?,?,?,?,?,?)");
		stmt.setString(1,b.getNome());
		stmt.setInt(2, b.getNumChamada());
		stmt.setString(3,b.getCurso());
		stmt.setString(4,b.getSerie());
		stmt.setString(5,b.getMateria());
		stmt.setString(6,b.getData());
		stmt.setInt(7, b.getNumPais());
		stmt.setInt(8, b.getQntFaltas());
		stmt.executeUpdate();
		JOptionPane.showMessageDialog(null,"Salvo com sucesso!!!");
		}catch(SQLException ex) {
			JOptionPane.showMessageDialog(null,"Erro ao salvar"+ex);
		}finally {
			TesteCon.closeConnection(connection,stmt);
		}
	}
	public List<AlunoBean> read(){
		Connection con  = TesteCon.getConnection();
		PreparedStatement  stmt = null;
		ResultSet rs = null;
		List<AlunoBean> alunos = new ArrayList<>();
		
		try {
			stmt = con.prepareStatement("SELECT * FROM faltas");
			rs = stmt.executeQuery();
			while(rs.next()) {
				AlunoBean aluno = new AlunoBean();
				aluno.setNome(rs.getString("nomeAluno"));
				aluno.setNumChamada(rs.getInt("numChamada"));
				aluno.setCurso(rs.getString("curso"));
				aluno.setSerie(rs.getString("serie"));
				aluno.setMateria(rs.getString("materia"));
				aluno.setData(rs.getString("data"));
				aluno.setNumPais(rs.getInt("numPais"));
				aluno.setQntFaltas(rs.getInt("qntFaltas"));
				alunos.add(aluno);
			}
		}catch(SQLException ex) {
			Logger.getLogger(AlunoDAO.class.getName()).log(Level.SEVERE,null, ex);
		}finally {
			TesteCon.closeConnection(con,stmt,rs);
		}
		return alunos;
	}
}
