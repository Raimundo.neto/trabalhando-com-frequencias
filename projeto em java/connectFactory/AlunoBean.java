package connectFactory;

public class AlunoBean {
	private String nome;
	private String curso;
	private int  numChamada;
	private String serie;
	private int numPais ;
	private String materia;
	private String data;
	private int qntFaltas;
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCurso() {
		return curso;
	}
	public void setCurso(Object object) {
		this.curso = (String) object;
	}
	public int getNumChamada() {
		return numChamada;
	}
	public void setNumChamada(int numChamada) {
		this.numChamada = numChamada;
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie =  serie;
	}
	public int getNumPais() {
		return numPais;
	}
	public void setNumPais(int string) {
		this.numPais = string;
	}
	public String getMateria() {
		return materia;
	}
	public void setMateria(String materia) {
		this.materia = materia;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public int getQntFaltas() {
		return qntFaltas;
	}
	public void setQntFaltas(int qntFaltas) {
		this.qntFaltas = qntFaltas;
	}
	
	

	
}
