package connectFactory;



import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TesteCon{
	private static final String driver = "com.mysql.jdbc.Driver";
	private static  final String url = "jdbc:mysql://localhost:3306/ProjetoEscolar";
	private static   final String user = "root";
	private static final  String pass = "password";
	public static Connection getConnection(){
		try {
			Class.forName(driver);
			return DriverManager.getConnection(url,user,pass);
		}catch(ClassNotFoundException | SQLException ex) {
			System.out.println(ex.getMessage());
			throw new RuntimeException("Erro na conexão");
			
		}
	}
	
	public static void closeConnection(Connection con) {
		try {
		if (con != null) {
			con.close();
			}
		}catch(SQLException ex){
			throw new RuntimeException("Imposibilidade de fechamento",ex);
		}
	}
	
	public static void closeConnection(Connection con, PreparedStatement stmt) {
		closeConnection(con);
		try {
		if (stmt != null) {
			stmt.close();
			}
		}catch(SQLException ex){
			throw new RuntimeException("Imposibilidade de fechamento",ex);
		}
	}
	
public static void closeConnection(Connection con, PreparedStatement stmt, ResultSet rs) {
		closeConnection(con,stmt);
		try {
				if(rs != null) {
					rs.close();
			}
		}catch(SQLException ex){
			throw new RuntimeException("Imposibilidade de fechamento",ex);
		}
	}
}
