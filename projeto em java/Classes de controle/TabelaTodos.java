package control;

import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import connectFactory.AlunoDAO;
import model.ModelTabelaTodos;
public class TabelaTodos extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//Instância da classe Tabela em tabela
	JPanel painel; //Composição de JPanel em painel
    JTable tabel; //Composição de JTable em tabel
    JScrollPane roll; //Composição de JScroollPane em roll
    AlunoDAO alunDao;
    ModelTabelaTodos tableTodos = new ModelTabelaTodos();
    public TabelaTodos() {
    	super("Todos os alunos");
    	 tabel = new JTable();
    	 tabel.setModel(tableTodos);
    	 setLocationRelativeTo(null);
    	 criarTabela();
    }
	public  void criarTabela(){ 
        painel = new JPanel(); 
        painel.setLayout(new GridLayout(1, 1)); 
        roll = new JScrollPane(tabel); 
        painel.add(roll);  
         
        getContentPane().add(painel); 
        setSize(1100, 170); 
        setVisible(true); 
    }
	public void MostrarTodos() {
		 alunDao = new AlunoDAO();
		 ModelTabelaTodos.lista = alunDao.read();
		 tableTodos.fireTableDataChanged();
	}
	public static void main(String[] args) {
		new TabelaTodos();
	}

}
