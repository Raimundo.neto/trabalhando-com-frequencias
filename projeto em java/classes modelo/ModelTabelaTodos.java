package model; //Pacote onde está a classe Tabela

//Importação de bibliotecas
import java.util.ArrayList;
import java.util.List;  
import javax.swing.table.AbstractTableModel;
//importação da classe Aluno
import connectFactory.AlunoBean;
/**
 * Essa classe é reponsável pelo <strong>inserimento dos dados </strong>passados pelo usuário, na tabela.
 * Logicamente, sem a mesma <span style="text-decoration:underline;">não é possivel que ocorra a parte mais importante do projeto</span>, que é a <strong>fácil</strong> visualização dos registros na tabela.
 * 
 * @author Everton Rocha e Eduarda Maria
 *
 */
public class ModelTabelaTodos extends AbstractTableModel{ //Herdando todos os recursos da AbstractTableModel

	private static final long serialVersionUID = 1L; //Indentificador de versão de serialização de uma classe
	/**
	 * Responsável pelo armazenamento de alunos
	 */
	public static List<AlunoBean> lista = new ArrayList<>(); //Instância de um ArrayList em lista

	//Definição das colunas da tabela com o array colunas
	/**
	 * Array responsável por definir o nome das colunas, onde cada nome vira uma coluna na tabela
	 */
	private String[ ] colunas = { "Nome" ,"Número", "Curso","Série","Num.Pais","Matéria","Data","Faltas" }; 
	
	//Retorno das colunas nomeadas respectivamente pelo conteúdo do array colunas 
	/**
	 * Método responsável por nomear as colunas da tabela
	 * @param int column
	 * @return colunas[column]
	 */
	public String getColumnName(int column) {
		return colunas[column] ;
	}
	
	//Sobrescrita dos métodos polimórficos da AbstractTableModel 
	
	/**
	 * Método responsável por definir a quantidade de colunas da tabela
	 * @return colunas.length
	 */
	@Override
	public int getColumnCount() { //Definição das quantidades de colunas
		return colunas.length; ////A quantidade de colunas será o seu próprio tamanho
	}

	/**
	 * Método responsável por definir a quantidade de linhas da tabela
	 * @return lista.size()
	 */
	@Override
	public int getRowCount() { //Definição das quantidades de linhas
		return lista.size(); //A quantidade de linhas será o seu próprio tamanho
	}

	/**
	 * Método responsável por pegar os valores das células da tabela.<br>
	 * Tendo como sintaxe uma estrutura elaborada switch case.
	 * @param int linha
	 * @param int coluna
	 * @return null
	 */
	public Object getValueAt(int linha, int coluna) {  //Pegar os valores da tabela
		
		switch(coluna) { //Escolha das colunas para serem preenchidas
		case 0:  //Primeira coluna, pois a primeira coluna inicia-se na coluna 0
			return lista.get(linha).getNome(); //Preencher a primeira coluna com o retorno do método getNome da classe Aluno
		case 1:
			return lista.get(linha).getNumChamada(); //Preencher a segunda coluna com o retorno do método getNumero da classe Aluno
		case 2:
			return lista.get(linha).getCurso(); //Preencher a terceira coluna com o retorno do método getCurso da classe Aluno
		case 3:
			return lista.get(linha).getSerie(); //Preencher a quarta coluna com o retorno do método getSerie da classe Aluno
		case 4:
			return lista.get(linha).getNumPais(); //Preencher a quinta coluna com o retorno do método getNumPais da classe Aluno
		case 5:
			return lista.get(linha).getMateria(); //Preencher a sexta coluna com o retorno do método getFaltaMateria da classe Aluno
		case 6:
			return lista.get(linha).getData(); //Preencher a oitava coluna com o retorno do método getDataFalta da classe Aluno
		case 7:
			return lista.get(linha).getQntFaltas(); //Preencher a nona coluna com o retorno do método getFaltas da classe Aluno
		}
		return null; //Caso nenhum  seja atendido o método retornará vazio(nada)
	}
}




	



