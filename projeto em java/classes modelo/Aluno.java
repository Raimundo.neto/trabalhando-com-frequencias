package model; //Pacote onde está a classe Aluno
/**
 * <meta charset="utf-8">Essa classe é responsável pelo inserimento e pegada de valores que um aluno pode possuir,como: nome,curso,serie etc.
 * É o modelo de dados que um aluno possui no projeto.
 * 
 * 
 * @author Everton Rocha e Eduarda Maria
 *
 */
	public class Aluno {
		//Definição dos atributos da classe 
		private String nome;
		private int numero;
		private String numPais;
		private String serie;
		private String curso;
		private String faltaMateria;
		private String dataFalta;
		private int faltas;
		
		//Encapsulamento dos atributos da classe
		//Os métodos 'set' irão atribuir valores ao atributo posterior ao seu nome
		//Os métodos 'get' irão retornar os valores dos atributos setados pelos métodos 'set'
		/**
		 * Esse método é responsável por pegar o valor atual do atributo nome 
		 * @return nome
		 */
		public String getNome() {
			return nome;
		}
		/**
		 * Método responsável por inserir valor no atributo nome
		 * @param String  nome
		 */
		public void setNome(String nome) {
			this.nome = nome;
		}
		/**
		 * Esse método é responsável por pegar o valor atual do atributo numero
		 * @return numero
		 */
		public int getNumero() {
			return numero;
		}
		/**
		 * Método responsável por inserir valor no atributo numero
		 * @param String nc
		 */
		public void setNumero(int nc) {
			this.numero = nc;
		}
		/**
		 * Esse método é responsável por pegar o valor atual do atributo curso
		 * @return curso
		 */
		public String getCurso() {
			return curso;
		}
		/**
		 * Método responsável por inserir valor no atributo curso
		 * @param String object
		 */
		public void setCurso(Object object) {
			this.curso = (String) object;
		}
		/**
		 * Esse método é responsável por pegar o valor atual do atributo serie
		 * @return  serie
		 */
		public String getSerie() {
			return serie;
		}
		/**
		 * Método responsável por inserir valor no atributo serie
		 * @param String serie
		 */
		public void setSerie(String indice) {
			this.serie =  indice;
		}
		/**
		 * Esse método é responsável por pegar o valor atual do atributo numPais
		 * @return numPais
		 */
		public String getNumPais() {
			return numPais;
		}
		/**
		 * Método responsável por inserir valor no atributo numPais
		 * @param String numPais
		 */
		public void setNumPais(String numPais) {
			this.numPais = numPais;
		}
		/**
		 * Esse método é responsável por pegar o valor atual do atributo faltaMateria
		 * @return faltaMateria
		 */
		public String getFaltaMateria() {
			return faltaMateria;
		}
		/**
		 * Método responsável por inserir valor no atributo materia
		 * @param String faltaMateria
		 */
		public void setFaltaMateria(String faltaMateria) {
			this.faltaMateria = faltaMateria;
		}
		/**
		 * Esse método é responsável por pegar o valor atual do atributo dataFalta
		 * @return dataFalta
		 */
		public String getDataFalta() {
			return dataFalta;
		}
		/**
		 * Método responsável por inserir valor no atributo dataFalta
		 * @param String data
		 */
		public void setDataFalta(String data) {
			this.dataFalta = data;
		}
		/**
		 * Esse método é responsável por pegar o valor atual do atributo faltas
		 * @return faltas
		 */
		public int getFaltas() {
			return faltas;
		}
		/**
		 * Método responsável por inserir valor no atributo faltas
		 * @param String faltas
		 */
		public void setFaltas(int faltas) {
			this.faltas = faltas;
		}
		
}