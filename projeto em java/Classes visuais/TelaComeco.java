package visual; //Pacote onde está a classe Menu

//Importações de bibliotecas
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * <meta charset='utf-8'> Essa classe é responsável por gerar a <span style="text-decoration:underline;">tela inicial </span>   do projeto.
 * Ela possui recursos pra o inserimento da logo, desde métodos que irão <strong>redimensionar</strong> a mesma. 
 * @author Everton Rocha e Eduarda Maria
 */

	public class TelaComeco extends JPanel implements ActionListener{
		//Herança de todos os recursos de JFrame e implementação de ActionListener na classe TelaComeco
		Menu menu ;  //Composição da classe Menu em menu
		public static JButton entrar = new JButton("Entrar"); // Instância de JButton em entrar recebendo como parâmetro a string Entrar
	    private static final long serialVersionUID = 1L; //Indentificador de versão de serialização da classe TelaComeco
	     private Image image; //Composição da biblioteca Image em image
	    public static  JFrame frame;//Composição de JFrame em frame

	    //public static  Image  imgEntrar;

	    		public TelaComeco() { //Definição do construtor
	            this.initialize(); //Inicialização dos componentes
	            setLayout(null); 
	            setVisible(true);//Visibilidade da Tela
	            add(entrar);//Adicionamento do botão na tela
	            entrar.setVisible(true); //Visibilidade dobotão
	            entrar.setBounds(235,295,200,45); //Posicionamento do botão
	            entrar.addActionListener(this);//Definindo para onde a execução do botão está direcionada
	            entrar.setBackground(this.menu.amarelo); //Definição da cor de fundo do botão;
	            entrar.setFont(this.menu.changeFont); //Definição da fonte do texto do botão;*/
	        }
	    		/**
	    		 * Esse método irá  selecionar a foto da logo do projeto.
	    		 * Informa também que a foto ocupará toda a tela
	    		 */	
	        protected void initialize() { //Criação de um método público e sem retorno chamado initialize, esse método irá chamar a imagem e também vai redimensioná-la 
	            this.image = this.getImage("blueCheck.png"); //Escolheu a imagem de fundo
	            
	            this.setLayout(new BorderLayout()); //Definir que a imagem será do tamanho da borda da tela
	        }
	        

	        /**
	         *  Esse método irá averiguar se o diretório mandado está correto,
	         * e caso a imagem não seja encontrada ele retornará nada.
	         * @param String path
	         * @return ImageIcon(imageURL).getResource()<br>
	         * Esse método irá averiguar se o diretório mandado está correto,
	         * e caso a imagem não seja encontrada ele retornará nada.
	         */
	        public Image getImage(String path) {
	            URL imageURL = getClass().getResource(path);
	            if (imageURL == null)
	                return null;

	            return new ImageIcon(imageURL).getImage();
	        }
	        
	        /**
	         * Esse método é responsável por desenhar a logo do projeto,
	         * definindo também sua altura e largura.
	         * @param Graphics g
	         */
	        public void paintComponent(Graphics g) {
	            super.paintComponent(g);
	            Dimension dDesktop = this.getSize();

	            double width = dDesktop.getWidth();
	            double height = dDesktop.getHeight();

	            Image background = new ImageIcon(this.image.getScaledInstance(
	                  (int) width, (int) height, 1)).getImage();

	            g.drawImage(background, 0, 0, this);
	        }
	        
		
			/**
			 * Esse método é responsável por adicionar um evento no botão chamado entrar.
			 * Esse evento é responsável pelo aparecimento da tela de preenchimento de dados,que a classe Menu.
			 * @param ActionEvent evt
			 */
	    	@Override
			public void actionPerformed(ActionEvent evt) {
				if(evt.getSource()==entrar) {
					menu = new Menu();
					frame.dispose();			
		}		
	}
			  public static void main(String[] args) { //Execução da classe TelaComeco juntamente com seus recursos
		            frame = new JFrame();
		            frame.setContentPane(new TelaComeco());
		            frame.setSize(700, 400);
		            frame.setVisible(true);
		            frame.setResizable(false);
		            frame. setLocationRelativeTo(null);
		        }
}
