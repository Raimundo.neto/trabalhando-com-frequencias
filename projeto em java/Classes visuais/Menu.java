package visual; //Pacote onde está a classe Menu

import model.Aluno; //Importação da classe Aluno
import control.Tabela; //Importação da classe Tabela

//Importações de bibliotecas
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import connectFactory.AlunoBean;
import connectFactory.AlunoDAO;
import connectFactory.TesteCon;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import control.TabelaTodos;
/**
 * <meta charset="utf-8">Essa classe é responsável por gerar a tela de <strong> preenchimento de dados</strong>, tendo como auxílio as classes Aluno , ConfirmarEnvio e Tabela.
 * Ela possui <strong>3 instâncias</strong>, que são das classes: Aluno , ConfirmarEnvio e Tabela.
 * A mesma é a classe principal do projeto, sendo ela o processo visual mais importante, onde o usuário irá inserir seus dados. Ela possui o auxílio de três botões: <strong>Mandar, apagar e ver planilha</strong>.
 * 
 * @author Everton Rocha e Eduarda Maria
 * 
 */
public class Menu extends JFrame implements ActionListener{ 
//Herança de todos os recursos de JFrame e implementação de ActionListener na classe Menu 
	
	private static final long serialVersionUID = 1L; //Indentificador de versão de serialização de uma classe
	//Criação dos componentes por instâncias
	JPanel painel = new JPanel();
	TabelaTodos tableTodos;
	Tabela tabel; //Composição de Tabela em tabel
	TabelaAlunos tabelinha; //Composição de TabelaAlunos em tabelinha
	static Font changeFont = new Font("Serif",Font.BOLD,18);
	static Font  fontButtons = new Font("Serif",Font.BOLD,15);
	static Font  fontWelcome = new Font("Serif",Font.ITALIC,15);
	static Color amarelo = new Color(228, 245, 255);
	static Color azul = new Color(56, 182, 255);
	public static JLabel nome = new JLabel("Nome Aluno: ");
	public static JLabel numChamada = new JLabel("N°Chamada:");
	public static JLabel curso = new JLabel("Curso:");
	public static JLabel serie = new JLabel("Série:");
	public static JLabel  numPais = new JLabel("Num. Pais:");
	public static JButton mandar = new JButton("Mandar");
	public static JButton verTodos = new JButton("Ver tudo");
	public static JButton vp = new JButton("Ver planilha");
	public static JTextField inputNome = new JTextField();
	public static JTextField  inputNumero = new JTextField(); 
	public static JComboBox<String> cs = new JComboBox<String>();
	public static JComboBox<String> sr = new JComboBox<String>();
	public static JTextField  np = new JTextField();
	public static JComboBox<String> atraso = new JComboBox<String>(); 
	public static JLabel formaAtraso = new JLabel("Tipo:");
	public static JButton apagar = new JButton("Apagar");
	public static ConfirmarEnvio mandarDados;
	public static JLabel data = new JLabel("Data: ");
	public static JFormattedTextField date = new JFormattedTextField(Mascara("##/##/####"));
	public static JLabel materia = new JLabel("Matéria:");
	public static JTextField boxMateria = new JTextField();
	public static JLabel faltas  = new JLabel("Qtd.Faltas:");
	public static JTextField inputFaltas = new JTextField();
	//public static JLabel welcome  = new JLabel("Seja bem-vindo(a)");
	//Construtor da classe Menu
	/**
	 * O construtor da classe Menu possibilita a titulação da tela criada pela mesma, execução das ações dos botões, adicionamento do JPanel e
	 * a impossibilidade de redimensionar a tela. 
	 */
	public Menu() {
		super("Gerenciamento de faltas"); //Definição do título da janela
		this.mandarDados = new ConfirmarEnvio(); //Instância da classe confirmarEnvio
		this.mandarDados.setVisible(false); //Visualização da classe confirmarEnvio
		add(painel); //Adicionamento do painel ao JFrame
		setVisible(true); //Visualização da tela
		placeComponents(painel); //Execução do método placeComponents
		setSize(600,180); //Tamanho da tela(JFrame)
		setLocationRelativeTo(null); //Surgimento da tela ao centro
		painel.setPreferredSize(new Dimension(580,200)); //Definindo o tamanho do painel
		pack(); //JFrame e JPanel ficarão colados, irão meio que se expremer entre sí
		setResizable(false); //Não será possível redimensionar a tela
		painel.setBackground(azul); //Cor de fundo da tela
		vp.addActionListener(this); //Definindo para onde a execução do botão está direcionada
		mandar.addActionListener(this); //Definindo para onde a execução do botão está direcionada
		this.mandarDados.btn2.addActionListener(this);//Definindo para onde a execução do botão está direcionada
		this.mandarDados.btn3.addActionListener(this);//Definindo para onde a execução do botão está direcionada
		apagar.addActionListener(this);//Definindo para onde a execução do botão está direcionada
		verTodos.addActionListener(this);
	}

	//Adicionamento dos componentes no painel
	/**
	 * Esse método é responsável pelo <strong>adicionamento</strong> e <strong>posicionamento</strong>  dos componentes no JPanel. <br>
	 * Podendo também difinir a cor dos componentes.
	 * @param Jpanel painel
	 */
	
	public static void placeComponents(JPanel painel) {
		
		painel.setLayout(null);
		painel.add(nome);
		painel.add(inputNome);
		painel.add(numChamada);
		painel.add(inputNumero);
		painel.add(mandar);
		painel.add(date);
		painel.add(curso);
		painel.add(serie);
		painel.add(numPais);
		painel.add(cs);
		painel.add(sr);
		painel.add(numPais);
		painel.add(np);
		painel.add(atraso);
		painel.add(vp);
		painel.add(apagar);
		painel.add(data);
		painel.add(materia);
		painel.add(boxMateria);
		painel.add(faltas);
		painel.add(inputFaltas);
		painel.add(verTodos);
		
		//Preenchimento dos JComboBox
		cs.addItem("");
		cs.addItem("Informática");
		cs.addItem("Enfermagem");
		cs.addItem("Guia de turismo");
		cs.addItem("Eventos");
		
		sr.addItem(null);
		sr.addItem("1° ano");
		sr.addItem("2° ano");
		sr.addItem("3° ano");
		
		//Posicionamento dos componentes
		curso.setBounds(339,10,80,25);
		curso.setForeground(Color.WHITE);
		curso.setFont(changeFont);
		cs.setBounds(400,10,180,25);
		serie.setBounds(345,40,80,25);
		serie.setForeground(Color.WHITE);
		serie.setFont(changeFont);
		sr.setBounds(400,40,180,25);
		nome.setBounds(7,10,120,25);
		nome.setForeground(Color.WHITE);
		nome.setFont(changeFont);
		inputNome.setBounds(123,10,180,25);
		numChamada.setBounds(12,40,135,25);
		numChamada.setForeground(Color.WHITE);
		numChamada.setFont(changeFont);
		inputNumero.setBounds(123,40,180,25);
		numPais.setBounds(27,70,100,25);
		numPais.setForeground(Color.WHITE);
		numPais.setFont(changeFont);
		np.setBounds(123,70,180,25);
		mandar.setBounds(8, 180, 88, 25);
		mandar.setBackground(amarelo);
		mandar.setFont(fontButtons);
		vp.setBounds(469,180, 115, 25);
		vp.setBackground(amarelo);
		vp.setFont(fontButtons);
		apagar.setBounds(104,180, 88, 25);
		apagar.setBackground(amarelo);
		apagar.setFont(fontButtons);
		data.setForeground(Color.WHITE);
		data.setBounds(70,100,100,25);
		data.setFont(changeFont);
		date.setBounds(123,100,180,25);
		faltas.setBounds(308,100,150,25);
		faltas.setForeground(Color.WHITE);
		faltas.setFont(changeFont);
		inputFaltas.setBounds(400,100,180,25);
		materia.setBounds(323,70,180,25);
		materia.setFont(changeFont);
		materia.setForeground(Color.WHITE);
		boxMateria.setBounds(400,70,180,25);
		verTodos.setBounds(360,180,100,25);
		verTodos.setForeground(Color.BLACK);
		verTodos.setBackground(amarelo);
		verTodos.setFont(fontButtons);
	}
	
	//Definição das ações dos botões ao serem clicados
	
	/**
	 * Esse método é responsável pelas ações de todos os botões da classe Menu e ConfirmarEnvio.
	 * @param ActionEvent evt
	 */
	 @Override
	public void actionPerformed(ActionEvent evt) {
		if(evt.getSource()==mandar) { //Essa condição irá fazer com que ao apertar o botão mandar a tela da classe ConfirmarEnvio apareça
			this.mandarDados=  new ConfirmarEnvio();
		if(this.mandarDados.isVisible() == false ) {
		this.mandarDados.setVisible(true);
		this.mandarDados.setResizable(false);
		}
	}
	
	if(evt.getSource()==vp) { //Essa condição irá fazer com que ao apertar o botão 'ver planilha', a tela da classe TabelaAlunos apareça
		tabelinha = new TabelaAlunos();
		tabelinha.setVisible(true);
		tabelinha.setLocationRelativeTo(null);
	}
	if(evt.getSource() == apagar) { //Esse condição irá apagar os valores dos JTextField da tela Menu
		inputNome.setText("");
		inputNumero.setText("");
		np.setText("");
		cs.setSelectedItem(null);
		sr.setSelectedItem(null);
		atraso.setSelectedItem(null);
		date.setText("");
		boxMateria.setText("");
		inputFaltas.setText(null);
		
	}
	if(evt.getSource() == this.mandarDados.btn2) { //Essa condição irá fazer com que ao apertar o botão 'sim' os dados sejam enviados para a planilha
		Aluno more = new Aluno(); //Essa instância permitirá acesso aos atributos e métodos da classe Aluno
		tabelinha = new TabelaAlunos(); //Essa instância permitirá que ao apertar o botão a janela da classe TabelaAlunos apareça
		tabelinha.setVisible(false); //A janela de TabelaAlunos será invisível
		
		
		
		//Setando valores nos atributos de Aluno com os valores dos JTextField e JComboBox
		String indice = (String) sr.getSelectedItem();
		more.setNome(inputNome.getText());
		more.setNumero(Integer.parseInt(inputNumero.getText()));
		more.setCurso(cs.getSelectedItem());
		more.setSerie(indice);
		more.setNumPais(np.getText());
		more.setFaltaMateria(boxMateria.getText());
		more.setDataFalta(date.getText());
		more.setFaltas(Integer.parseInt(inputFaltas.getText()));
		
		AlunoBean ab = new AlunoBean();
		AlunoDAO ad  = new AlunoDAO();
		ab.setNome(inputNome.getText());
		ab.setCurso(cs.getSelectedItem());
		ab.setNumChamada(Integer.parseInt(inputNumero.getText()));
		ab.setSerie(indice);
		ab.setNumPais(Integer.parseInt(np.getText()));
		ab.setMateria(boxMateria.getText());
		ab.setData(date.getText());
		ab.setQntFaltas(Integer.parseInt(inputFaltas.getText()));
		ad.create(ab);
		
		tabelinha.addAluno(more); //Método addAluno está sendo executado com o parâmetro 'more', onde o mesmo é um objeto de Aluno
		
		//Definindo valores vazios para os JTextField e JComboBox no momento que os dados são enviados
		inputNome.setText("");
		inputNumero.setText("");
		np.setText("");
		cs.setSelectedItem(null);
		sr.setSelectedItem(null);
		atraso.setSelectedItem(null);
		date.setText("");
		boxMateria.setText("");
		inputFaltas.setText(null);

		mandarDados.dispose();
		
		//Fechamento da classe ConfirmarEnvio
	
	}
	
	if(evt.getSource()==mandarDados.btn3) { //Ao apertar no btn3(botao 'não') a tela será fechada
		this.mandarDados.dispose();
	}
	
	if(evt.getSource()==verTodos) {
		tableTodos = new TabelaTodos();
		tableTodos.setVisible(true);
		tableTodos.MostrarTodos();
	}
}
	 public static MaskFormatter Mascara(String Mascara){
	        
	        MaskFormatter F_Mascara = new MaskFormatter();
	        try{
	            F_Mascara.setMask(Mascara); //Atribui a mascara
	            F_Mascara.setPlaceholderCharacter(' '); //Caracter para preencimento 
	        }
	        catch (Exception excecao) {
	        	excecao.printStackTrace();
	        } 
	        return F_Mascara;
	 } 
	//Execução da classe Menu
	 public static void main(String[] args) {
		new Menu();
	}
}
