package visual; //Pacote onde está a classe TabelaAlunos
//Importação das bibliotecas para criar uma tabela
import java.awt.GridLayout;
import java.awt.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.mysql.fabric.xmlrpc.base.Array;

import connectFactory.AlunoBean;
import connectFactory.TesteCon;
import control.Tabela; //Importação da classe Tabela
import model.Aluno; //Importação da classe Aluno
 
/**
 * <meta charset='utf-8'> Essa classe é responsável por gerar os recursos necessários para a <span style="text-decoration:underline;">visualização</span> 
 * da tabela de visualização de faltas.<br/>
 * Essa classe possui uma instância com a classe <strong>Tabela</strong>, <br/>onde essa instância possibilita que a tabela tenha um <strong>modelo<strong>.
 * 
 *@author Everton Rocha e Eduarda Maria
 */
public class TabelaAlunos extends JFrame { //Classe TabelaAlunos herdando todos os recursos de JFrame
	private static final long serialVersionUID = 1L;
	Tabela tabela = new Tabela(); //Instância da classe Tabela em tabela
	JPanel painel; //Composição de JPanel em painel
    JTable tabel; //Composição de JTable em tabel
    JScrollPane roll; //Composição de JScroollPane em roll

    /**
     * O construtor da classe TabelaAlunos, permite definir o título, modelo e localização da tabela ao aparecer. 
     */
    public TabelaAlunos() { //Criação do construtor da classe TabelaAlunos
        super ("Planilha de Alunos"); //Definição do título da janela
        tabel = new JTable(); //Instância de JTable
        tabel.setModel(tabela); //Definição do modelo da tabela, que será do modelo mandado na classe Tabela
        setLocationRelativeTo(null); //A classe não poderá ser redimensionada pelo usuário
         criarTabela(); //Execução do método criarTabela
    }
    
    
/**
 * Esse método é responsável pela definição da características da tabela, como: tamanho, tamanho da grade, e a adição de um scroll caso
 * a quantidade de registros passe do tamanho da tela
*/
    public  void criarTabela(){ //Criação de um método público e sem retorno chamado criarTabela
        painel = new JPanel(); //Instância de JPanel em painel
        painel.setLayout(new GridLayout(1, 1)); //Definição da grid e sua expessura no JPanel
        roll = new JScrollPane(tabel); //Instância de JScrollPane, resultando num scroll caso os registros da tabela excedam o tamanho da tela 
        painel.add(roll); //Adicionamento do objeto roll no JPanel 
         
        getContentPane().add(painel); //Adiconamento do JPanel com seus recursos, como: grid, scroll etc.
        setSize(1100, 170); //Tamanho da tabela
        setVisible(true); //Visualização da tela 
    }
   /**
    *Esse método irá adicionar valores nos atributos da classe Aluno, permitindo que os dados do aluno seja inserido na tabela.
    *Esse método permitirá também que a tabela seja atualizada a cada novo registro
    * @param  Aluno aluno
 * @return 
    */
    public void addAluno(Aluno more) { //Método chamadoAddAluno que irá inserir um novo aluno na Tabela
		Tabela.lista.add(more);
		tabela.fireTableDataChanged();
    }
    public static void main(String[] args) {
    	new TabelaAlunos();
    }

}


