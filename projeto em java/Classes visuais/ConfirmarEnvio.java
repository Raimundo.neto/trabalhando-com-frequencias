package visual; //Pacote onde está a classe Menu

//Importações de bibliotecas
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Essa classe é responsável por ao apertar o botão 'enviar ' gere uma tela de confirmação de envio.
 * @author Everton Rocha 
 * @author Eduarda Maria
 *
 */
public  class ConfirmarEnvio extends JFrame implements ActionListener{ 
	//Herança de todos os recursos de JFrame e implementação de ActionListener na classe ConfirmarEnvio 

	//Criação dos componentes por instâncias
	private static final long serialVersionUID = 1L;
	static Color darkBlue = new Color(56, 182, 255);
	static Color lightYellow = new Color(228, 245, 255);
	public static Font style = new Font("Serif",Font.BOLD,27);
	public static JButton btn2 = new JButton("Sim");
	public static JButton btn3 = new JButton("Não");
	JPanel painel = new JPanel();
	public static JLabel confirmar = new JLabel("Tem certeza ?");
	
	//Construtor da classe
	/**
	 * O construtor da classe ConfirmarEnvio possiblita a titulação da tela gerada pela mesma, o tamanho da tela e a impossibilidade de redimensionar a tela ao seu surgimento.
	 */
	public ConfirmarEnvio(){	
		super("Certificação de envio"); //Definição do título da janela
		add(painel); //Adicionamento do painel ao JFrame
		setVisible(true);//Visualização da tela
		placeComponents(painel); //Execução do método placeComponents
		setSize(600,200); //Tamanho da tela(JFrame)
		setLocationRelativeTo(null); //Surgimento da tela ao centro
		painel.setPreferredSize(new Dimension(400,120)); //Definindo o tamanho do painel
		pack(); //JFrame e JPanel ficarão colados, irão meio que se expremer entre sí
		setResizable(false); //Não será possível redimensionar a tela
		painel.setBackground(darkBlue); //Cor de fundo da tela
		btn3.addActionListener(this); //Definindo para onde a execução do botão está direcionada
		btn2.addActionListener(this); //Definindo para onde a execução do botão está direcionada
	}
	
	//Adicionamento dos componentes no painel
	/**
	 * Esse método possibilita o inserimento, e posicionamento dos componentes no painel, possibilitando também sua coloração.
	 * @param Jpanel painel
	 */
	public static void placeComponents(JPanel painel) {
		painel.setLayout(null);
		painel.add(btn2);
		painel.add(confirmar);
		painel.add(btn3);
		
		btn2.setBounds(60, 95, 82, 25);
		btn2.setBackground(lightYellow);
		confirmar.setBounds(134,2,172,40);
		confirmar.setFont(style);
		confirmar.setForeground(Color.WHITE);
		btn3.setBounds(290,95,82,25);
		btn3.setBackground(lightYellow);
		
	}
	public static void main(String[] args) { //Execução da classe ConfirmarEnvio
		new ConfirmarEnvio();
	}
	
	/**
	 * Esse método é responsável pelas ações dos botões
	 * 
	 * @param ActionEvent e 
	 * 
	 */
	public void actionPerformed(ActionEvent e) { //Existência obrigatória desse método polimórfico devido a classe ser implementada pelo ActionListener
		// TODO Auto-generated method stub
		
	}	
}





