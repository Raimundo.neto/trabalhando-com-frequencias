DROP SCHEMA IF EXISTS ProjetoEscolar;
CREATE SCHEMA IF NOT EXISTS ProjetoEscolar;

CREATE TABLE ProjetoEscolar.faltas(
	id INT primary key auto_increment,
	nomeAluno VARCHAR(100) NOT NULL,
	numChamada INT(2) NOT NULL,
	curso VARCHAR(13) NOT NULL,
	serie VARCHAR(6) NOT NULL,
	materia VARCHAR(60) NOT NULL,
	data VARCHAR(10) NOT  NULL,
	numPais INT(9),
	qntFaltas INT NOT NULL
);